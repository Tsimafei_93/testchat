﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MessengerDB.Model
{
    public class Chat
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Message> Messages { get; set; } = new HashSet<Message>();

        public virtual ICollection<User> Users { get; set; } = new HashSet<User>();
    }
}