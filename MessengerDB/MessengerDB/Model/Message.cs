﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MessengerDB.Model
{
    public class Message
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("ChatId")]
        public virtual Chat Chat { get; set; }
        public string TextMessage { get; set; }
        public DateTime MessageTime { get; set; }
        public bool IsDeleted { get; set; }
    }
}
