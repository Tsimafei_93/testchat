﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessengerDB.Model
{
    public class Role
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<User> Users { get; set; } = new HashSet<User>();
    }
}
