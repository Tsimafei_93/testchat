﻿using ClassLibrary7.Repositories.Interfaces;
using MessengerDB;
using System.Collections.Generic;
using System.Linq;


namespace ClassLibrary7.Repositories
{
    public class GenericRepo<T> : IGenericRepo<T>
        where T : class
    {
        public MessengerDbContext _context;
        public GenericRepo(MessengerDbContext context)
        {
            _context = context;
        }
        public T Find(int id)
        {
            return _context.Find<T>(id);
        }

        public List <T> ReadAll()
        {
            return _context.Set<T>().ToList();
         }
    }
}
