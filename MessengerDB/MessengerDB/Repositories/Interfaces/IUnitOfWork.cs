﻿using ClassLibrary7.Repositories;
using MessengerDB.Model;

namespace ClassLibrary.Repositories.Interfaces
{
    public interface IUnitOfWork
    {
        GenericRepo<Role> Roles { get; }

        GenericRepo<User> Users { get; }

        public void SaveChanges();
    }
}
