﻿using System.Collections.Generic;
using System.Linq;

namespace ClassLibrary7.Repositories.Interfaces
{
    public interface IGenericRepo<T>
        where T : class
    {
        public T Find(int id);
        public List<T> ReadAll();
    }
}
