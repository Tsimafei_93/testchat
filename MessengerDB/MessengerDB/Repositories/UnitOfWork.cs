﻿using ClassLibrary.Repositories.Interfaces;
using ClassLibrary7.Repositories;
using MessengerDB;
using MessengerDB.Model;

namespace ClassLibrary.Repositories
{
    public class UnitOfWork : IUnitOfWork


    {

        public readonly MessengerDbContext _context;
        public UnitOfWork(MessengerDbContext context)
        {
            _context = context;
        }

        public GenericRepo<User> _users;
        public GenericRepo<User> Users
        {
            get => _users is null ? _users = new GenericRepo<User>(_context) : _users;
        }

        public GenericRepo<Role> _role;
        public GenericRepo<Role> Roles
        {
            get => _role is null ? _role = new GenericRepo<Role>(_context) : _role;
        }



        public void SaveChanges()
        {
            _context.SaveChanges();
        }

    }
}
