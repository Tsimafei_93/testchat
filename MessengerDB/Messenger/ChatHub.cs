﻿using Microsoft.AspNetCore.SignalR;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Messenger
{
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
    public class ChatHub : Hub
    {
        public async Task Send(string message)
        {
            await Clients.All.SendAsync("MessageSent",
                message,
                Context.ConnectionId,
                new List<Person> {
                    new Person { FirstName = "Alex", LastName ="Petrov"},
                    new Person { FirstName = "Petr", LastName = "Alexeev"}
                      }
        );
        }


    }
}
