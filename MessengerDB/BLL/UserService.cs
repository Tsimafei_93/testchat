﻿using ClassLibrary.Repositories.Interfaces;
using MessengerDB.Model;
using Services.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _uow;

        public UserService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public User Get(int id)
        {
           return _uow.Users
                .Find(id);
        }

        public List<User> GetAll()
        {
            return _uow.Users
                .ReadAll().ToList();
        }


    }
}
