﻿using MessengerDB.Model;
using System.Collections.Generic;
using System.Linq;

namespace Services.Interfaces
{
    public interface IUserService
    {
        public List<User> GetAll();
        public User Get(int id);
    }
}
